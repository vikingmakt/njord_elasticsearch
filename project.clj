(defproject njord_elasticsearch "0.1.4"
  :description "Njord ElasticSearch Service"
  :url "https://gitlab.com/vikingmakt/njord_elasticsearch"
  :license {:name "BSD-2-Clause"}
  :dependencies [[br.com.vikingmakt/augustus "0.1.8"]
                 [cheshire "5.8.0"]
                 [com.novemberain/langohr "3.7.0"]
                 [http-kit "2.2.0"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jdmk/jmxtools
                                                    com.sun.jmx/jmxri]]
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.logging "0.4.0"]
                 [utcode "0.1.7"]]
  :main ^:skip-aot njord-elasticsearch.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
