(ns njord-elasticsearch.request
  (:require [augustus.async :as async]
            [augustus.future :as afuture]
            [cheshire.core :as json]
            [clojure.string :as string]
            [clojure.tools.logging :as log]
            [langohr.basic :as basic]
            [langohr.channel :as channel]
            [langohr.consumers :as consumers]
            [njord-elasticsearch.parser :as parser]
            [org.httpkit.client :as http]
            [utcode.core :as utcode])
  (:import java.io.IOException
           [java.net ConnectException SocketException SocketTimeoutException]
           java.util.Calendar)
  (:gen-class))

(defn- get-timestamp []
  (.getTimeInMillis ^Calendar (Calendar/getInstance)))

(defn- time-diff [d1 d2]
  (/ (double (- d2 d1)) 1000))

(defn log-request [{:keys [body resp]}]
  (log/info (format "Elastic op %s %s returned %s in %s seconds" (or (:method body) "GET") (:url body) (:status resp) (:request-time resp))))

(defn respond [{:keys [ack ch config resp headers] :as msg}]
  (log-request msg)
  (let [headers (assoc headers :njord-response true)]
    (->> {:content-encoding "UTF-8"
          :headers (parser/map->headers headers)}
         (basic/publish ch (get-in config [:exchange :topic]) "" (utcode/encode resp)))
    (ack true)))

(defn parse-status [{:keys [error] :as resp}]
  (if (nil? error)
    resp
    (assoc resp
           :error (parser/throwable->msg error)
           :status (condp #(instance? % %2) error
                     ConnectException 503
                     IOException 504
                     SocketTimeoutException 408
                     SocketException 504
                     500))))

(defn on-fetch [msg resp]
  (->> (let [request-time (time-diff (:before-request msg) (get-timestamp))
             {:keys [error body status]} (parse-status resp)]
         (if-not error
           {:status status
            :payload (json/parse-string body)
            :request-time request-time
            :err nil}
           {:status status
            :payload nil
            :request-time request-time
            :err error}))
       (assoc msg :resp)
       (async/add-callback respond)))

(defn fetch [{:keys [request] :as msg}]
  (let [msg (assoc msg :before-request (get-timestamp))]
    (http/request request #(on-fetch msg %))))

(defn prepare [{:keys [body] :as msg}]
  (->> (if (:payload body) {:body (json/generate-string (:payload body))})
       (merge {:url (:url body)
               :headers {"Content-Type" "application/json"}
               :method (if-let [m (:method body)] (keyword (string/lower-case m)) :get)})
       (assoc msg :request)
       (async/add-callback fetch)))

(defn validate-url [{:keys [url]}]
  (and (string? url) (not-empty url)))

(defn validate-method [{:keys [method]}]
  (or (nil? method)
      (some #{method} ["GET" "POST" "PUT" "DELETE" "HEAD"])))

(defn validate-payload [{:keys [payload]}]
  (or (nil? payload)
      (and (map? payload)
           (not-empty payload))))

(defn validate [{:keys [ack body] :as msg}]
  (if (and (validate-url body)
           (validate-method body)
           (validate-payload body))
    (async/add-callback prepare msg)
    (ack true)))

(defn parse-body [^bytes body]
  (try
    (utcode/decode (new String body "UTF-8"))
    (catch Throwable e)))

(defn on-msg [config ch {:keys [headers delivery-tag]} body]
  (async/add-callback
   validate
   {:body (parse-body body)
    :ch ch
    :headers (parser/headers->map headers)
    :config config
    :ack (afuture/new-future (fn [r] (basic/ack ch delivery-tag)))}))

(defn start [conn config]
  (let [queue (get-in config [:services :request :queue])]
    (doto (channel/open conn)
      (basic/qos (:prefetch config))
      (consumers/subscribe queue (fn [& args] (apply async/add-callback on-msg config args)))))
  (log/info "Started"))
