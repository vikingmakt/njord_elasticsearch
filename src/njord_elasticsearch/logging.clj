(ns njord-elasticsearch.logging
  (:require [clojure.string :refer [join]])
  (:import java.lang.IllegalArgumentException
           [org.apache.log4j Level Logger])
  (:gen-class))

(def ^:private valid-levels
  {:ALL Level/ALL
   :DEBUG Level/DEBUG
   :INFO Level/INFO
   :WARN Level/WARN
   :ERROR Level/ERROR
   :TRACE Level/TRACE})

(def ^:private arg-err-msg (format "\"log-level\" option must be %s" (join ", " (keys valid-levels))))

(defn- get-by-name [name]
  (or
   (get valid-levels name)
   (throw (new IllegalArgumentException ^String arg-err-msg))))

(defn set-level [name]
  (if-not (nil? name)
    (.setLevel (Logger/getRootLogger) (get-by-name name))))
