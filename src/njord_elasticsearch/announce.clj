(ns njord-elasticsearch.announce
  (:require [augustus.async :as async]
            [clojure.tools.logging :as log]
            [langohr.channel :as channel]
            [langohr.exchange :as exchange]
            [langohr.queue :as queue])
  (:gen-class))

(defn init-services [ch config services fut]
  (if (seq services)
    (let [serv (first services)
          topic (get-in config [:exchange :topic])
          queue (-> serv val :queue)
          rk (-> serv val :rk)]
      (queue/declare ch queue {:durable true :auto-delete false})
      (log/info (format "QUEUE - %s" queue))
      (queue/bind ch queue topic {:routing-key rk})
      (log/info (format "QUEUE BIND - %s:%s -> %s" topic rk queue))
      (recur ch config (dissoc services (key serv)) fut))
    (do
      (channel/close ch)
      (fut true))))

(defn bind-headers [ch {:keys [exchange]}]
  (let [topic (:topic exchange)
        headers (:headers exchange)]
    (exchange/bind ch headers topic {:routing-key "#"})
    (log/info (format "EXCHANGE BIND - %s -> %s" topic headers))))

(defn declare-headers [ch {:keys [exchange]}]
  (let [headers (:headers exchange)]
    (exchange/declare ch headers "headers" {:durable true})
    (log/info (format "EXCHANGE HEADERS - %s" headers))))

(defn declare-topic [ch {:keys [exchange]}]
  (let [topic (:topic exchange)]
    (exchange/declare ch topic "topic" {:durable true})
    (log/info (format "EXCHANGE TOPIC - %s" topic))))

(defn init [conn config fut]
  (let [topic (get-in config [:exchange :topic])
        headers (get-in config [:exchange :headers])]
    (doto (channel/open conn)
      (declare-topic config)
      (declare-headers config)
      (bind-headers config)
      (init-services config (:services config) fut))))
