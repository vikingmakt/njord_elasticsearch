(ns njord-elasticsearch.core
  (:require [augustus.async :as async]
            [augustus.future :as afuture]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [langohr.core :as rmq]
            [njord-elasticsearch.announce :as announce]
            [njord-elasticsearch.logging :refer [set-level]]
            [njord-elasticsearch.request :as service-request])
  (:gen-class))

(def settings
  {:exchange {:topic "njord_elasticsearch"
              :headers "njord_elasticsearch_headers"}
   :services {:request {:queue "njord_elasticsearch_request"
                        :rk "njord.elasticsearch.request"}}})

(defn services [conn config result]
  (async/add-callback service-request/start conn config))

(defn rmq-connect [config]
  (let [conn (rmq/connect (:rmq config))]
    (log/info (format "RMQ - Connected to %s:%s" (-> conn .getAddress .getHostAddress) (.getPort conn)))
    (announce/init conn config (afuture/new-future #(services conn config %)))))

(defn load-config []
  (let [file (io/as-file "settings.edn")]
    (if (.exists file)
      (merge settings (edn/read-string (slurp file)))
      (log/error "Cannot load settings"))))

(defn -main [& args]
  (when-let [config (load-config)]
    (set-level (:log-level config))
    (log/info "Starting service")
    (async/add-callback rmq-connect config)))
