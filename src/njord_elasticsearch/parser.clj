(ns njord-elasticsearch.parser
  (:import com.rabbitmq.client.LongString)
  (:gen-class))

(defn headers->map [h]
  (into {} (map (fn [[k v]] [(keyword k) (if (instance? LongString v) (str v) v)]) h)))

(defn map->headers [m]
  (into {} (map (fn [[k v]] [(name k) v])) m))

(defn throwable->msg [e]
  {:str (str e)
   :class (.getCanonicalName (class e))
   :msg (.getMessage ^Throwable e)})
