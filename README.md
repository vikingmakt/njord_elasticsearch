# Njord Elasticsearch

## SETUP

1. Copy the file **settings.edn.example** to **settings.edn**
2. Adjust the **settings.edn** as needed (RabbitMQ uri, logging level, prefetch count)
3. Run the command **lein run**

## DEPLOY

1. Run **lein uberjar**
2. Copy the jar generated in **target/uberjar/njord_elasticsearch-\<version\>-standalone.jar** and the **settings.edn** to the server which the service will be running
3. Run **java -jar njord_elasticsearch-\<version\>-standalone.jar**

## API

* All messages in this service must be in UTCode format

### REQUEST

### INPUT

* exchange -> **njord_elasticsearch**
* rk -> **njord.elasticsearch.request**
* headers -> Any header you need in the response
* body ->
  * url -> Full url to elasticsearch
  * method -> [GET (default), POST, PUT, DELETE]
  * payload -> Body to send to Elasticsearch

```clojure
{
    :url "http://localhost:9200/test/type1/1/_update"
    :method "POST"
    :payload {
		:script {
			:source "ctx._source.counter += params.count"
			:lang "painless"
			:params {
				:count 4
			}
		}
	}
}
```

### OUTPUT

* exchange -> **njord_elasticsearch_headers**
* headers -> Copy of headers received + **njord-response**: true
* body ->
  * status -> HTTP code
  * payload -> Result body
  * err -> Any error that ocurred
	  * str -> String of the error
	  * class -> Error class type
	  * msg -> Error message

```clojure
{
	:status 200
	:payload {
		:_shards {
			:total 0
			:successful 0
			:failed 0
		}
		:_index "test"
		:_type "type1"
		:_id "1"
		:_version 6
		:result "noop"
	}
   :err nil
}
```

## CHANGELOG

|Version|Description              |
|-------|-------------------------|
|0.1.2  |Handle request exceptions|
|0.1.1  |Logging level by settings|
|0.1.0  |Request actor            |
